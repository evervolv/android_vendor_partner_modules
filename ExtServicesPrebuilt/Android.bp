//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_ExtServicesPrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_ExtServicesPrebuilt_license",
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-ISC",
        "SPDX-license-identifier-MIT",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.extservices",
    apex_name: "com.android.extservices",
    owner: "google",
    overrides: [
        "com.android.extservices",
        "com.android.extservices.gms",
    ],
    filename: "com.google.android.extservices.apex",
    set: "com.google.android.extservices.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "GoogleExtServicesConfigOverlay",
    ],
}

module_apex_set {
    name: "com.google.android.extservices_compressed",
    apex_name: "com.android.extservices",
    owner: "google",
    overrides: [
        "com.android.extservices",
        "com.android.extservices.gms",
    ],
//    filename: "com.google.android.extservices.capex",
    set: "com.google.android.extservices_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "GoogleExtServicesConfigOverlay",
    ],
}

prebuilt_etc {
    name: "GoogleExtServices_permissions.xml",
    src: "GoogleExtServices_permissions.xml",
    sub_dir: "permissions",
}

// for devices not supporting updatable APEX or low RAM devices
override_apex {
    name: "com.android.extservices.gms",
    base: "com.android.extservices",
    logging_parent: "com.google.android.modulemetadata",
    apps: ["GoogleExtServices"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_android_app_set"],
}

// non updatable version included in com.android.extservices.gms
module_android_app_set {
    name: "GoogleExtServices",
    owner: "google",
    set: "GoogleExtServices.apks",
    privileged: true,
    overrides: ["ExtServices",],
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "GoogleExtServices_permissions.xml",
        "GoogleExtServicesConfigOverlay",
    ],
}
